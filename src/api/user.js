import utils from "@/api/utils";

function toFancyUser(user) {
    return {
        id: user.id,
        username: user.name,
        is_admin: user.is_admin,
        tokens: (user.tokens ? user.tokens.map((token) => {
            return {
                id: token.id,
                description: token.description,
                expires: new Date(token.expires),
                is_me: token.is_me
            }
        }) : null)
    }
}

export default {

    async askToken(user, pwd, expires, desc = "A browser login") {
        return utils.fetch("/auth", {
            method: 'POST',
            body: {name: user, password: pwd, expires: expires, description: desc}
        })
            .then((res) => {
                if (res.value) return res.value
                else if (res.error === 'unknown user') throw {type: 'user', text: 'Invalid login', e: res.error}
                else if (res.error === 'invalid credentials') throw {type: 'user', text: 'Invalid password', e: res.error}
                throw {type: 'internal', e: res.error}
            })
    },

    async deleteToken() {
        return utils.fetch("/auth", {method: 'DELETE'})
            .then((res) => {
                if (res.value) return res.value
                throw {type: 'internal', e: res.error}
            })
    },

    async deleteTokenById(tokenId) {
        return utils.fetch("/auth/byid", {
            method: 'DELETE',
            body: tokenId
        })
            .then((res) => {
                if (res.value) return res.value
                throw {type: 'internal', e: res.error}
            })
    },

    async getUser(userId) {
        return utils.fetch(`/users/${userId}`, {method: 'GET'})
            .then((res) => {
                if (res.value) return toFancyUser(res.value)
                throw {type: 'internal', e: res.error}
            })
    },

    async getUsers() {
        return utils.fetch(`/users/`)
            .then((res) => {
                if (res.value) return res.value.users.map(user => toFancyUser(user))
                throw {type: 'internal', e: res.error}
            })
    },

    async createUser(name, pwd, admin) {
        return utils.fetch(`/users/`, {
            method: 'POST', body: {
                name: name,
                password: pwd,
                is_admin: admin
            }
        })
            .then((res) => {
                if (res.value) return res.value.id
                else if (res.error === 'name already exists')
                    throw {type: 'user', text: 'This user already exists', e: res.error}
                else if (res.error === 'invalid data')
                    throw {type: 'user', text: 'Wrong password requirements', e: res.error}
                throw {type: 'internal', e: res.error}
            })
    },

    async editUser(id, props) {
        return utils.fetch(`/users/${id}/`,
            {
                method: 'PATCH',
                body: {
                    name: props.username,
                    password: props.password,
                    is_admin: props.is_admin
                }
            }
        ).then(res => {
            if (res.value) return res.value
            else if (res.error === 'invalid data')
                throw {type:'user', text: 'No user data was modified', e: res.error}
            else if (res.error === 'unknown user')
                throw {type:'internal', text: 'This user doesn\'t exists', e: res.error}
            throw {type: 'internal', e: res.error}
        })
    },

    async deleteUser(id) {
        return utils.fetch(`/users/${id}`, {method:'DELETE'})
            .then(res => {
                if (res.value) return res.value
                throw {type: 'internal', e: res.error}
            })
    }
}