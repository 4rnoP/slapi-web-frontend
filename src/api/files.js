import utils from "@/api/utils";

function toFancyFile(file) {
    return {
        id: file.id,
        userid: file.userid,
        name: file.name,
        tags: file.tags,
        upload_date: new Date(file.upload_date)
    }
}

export default {
    async getFiles () {
        return utils.fetch("/files/")
            .then((res) => {
                if (res.value) return res.value.files.map(file => toFancyFile(file))
                throw {type: 'internal', e: res.error}
            })
    },

    async getFile (fileId) {
        return utils.fetch(`/files/${fileId}/`, {method: 'GET'})
            .then((res) => {
                if (res.value) return toFancyFile(res.value)
                throw {type: 'internal', e: res.error}
            })
    },

    async downloadFile (fileId) {
        return utils.fetch(`/files/${fileId}/download/`, {method: 'GET'})
            .then((res) => {
                if (res.constructor.name === 'Blob') return res
                throw {type: 'internal', e: res.error}
            })
    },

    async uploadFile (file, name, tags) {
        let newFile = new File([file], name, {'type': 'application/zip'})
        const formData = new FormData();
        formData.append('file', newFile)
        formData.append('tags', JSON.stringify(tags))
        return utils.fetch("/files/", {method: 'POST', body: formData})
            .then((res) => {
                if (res.value) return res.value.id
                else if (res.error === 'name already exists'){
                    throw {type: 'user', text: 'A file with identical name already exists', e: res.error}
                } else if (res.error === 'invalid data'){
                    throw {type: 'user', text: 'Wrong file format', e: res.error}
                } throw {type: 'internal', e: res.error}
            })
    },

    async deleteFile (fileId) {
        return utils.fetch(`/files/${fileId}`, {method: 'DELETE'})
            .then((res) => {
                if (res.value) return res.value
                throw {type: 'internal', e: res.error}
            })
    }
}