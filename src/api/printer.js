import utils from "@/api/utils";

const actions = {
    powerOn: {method: 'POST', path: 'power'},
    powerOff: {method: 'DELETE', path: 'power'},
    startPrint: {method: 'POST', path: 'jobs'},
    pausePrint: {method: 'PATCH', path: 'jobs/pause'},
    resumePrint: {method: 'PATCH', path: 'jobs/resume'},
    cancelPrint: {method: 'DELETE', path: 'jobs'},
    startCalibration: {method :'POST', path: 'calibration'},
    stopCalibration: {method: 'DELETE', path: 'calibration'}
}

function toFancyPrinterStatus(s){
    if (s?.powered?.printing?.start_date){
        s.powered.printing.start_date = new Date(s.powered.printing.start_date)
        s.powered.printing.end_date = new Date(s.powered.printing.end_date)
    }
    return s
}

function toFancyJobs(j){
    j.forEach((job, index, array) =>
        Object.assign(array[index], {
            start_date: new Date(job.start_date),
            end_date: new Date(job.end_date)
        }))
    return j
}

export default {
    async getPrinters () {
        return utils.fetch("/printers/", {method: 'GET'})
            .then((res) => {
                if (res.value) return res.value.printers
                throw {type:'internal', e: res.error}
            })
    },

    async getPrinterInfo (printerId) {
        return utils.fetch(`/printers/${printerId}/`, {method: 'GET'})
            .then((res) => {
                if (res.value) return res.value
                throw {type:'internal', e: res.error}
            })
    },

    async newPrinter (name, socket, properties) {
        return utils.fetch("/printers/", {method: 'POST', body: {
                name: name,
                socket_address: socket,
                properties: properties
            }})
            .then((res) => {
                if (res.value) return res.value.id
                else if (res.error === 'name already exists') throw {type:'internal', e: res.error, text: 'A printer with this name already exists'}
                throw {type:'internal', e: res.error}
            })
    },

    async editPrinter (printerId, name, socket, properties) {
        return utils.fetch(`/printers/${printerId}`, {method: 'PATCH', body: {
                name: name,
                socket_address: socket,
                properties: properties}
        }).then((res) => {
                if (res.value) return res.value
                throw {type:'internal', e: res.error}
            })
    },

    async deletePrinter (printerId) {
        return utils.fetch(`/printers/${printerId}`, {method: 'DELETE'})
            .then((res) => {
                if (res.value) return res.value
                throw {type:'internal', e: res.error}
            })
    },

    async moveCalibration (printerId, value) {
        return utils.fetch(`/printers/${printerId}/calibration`, {method: 'PATCH', body: {
                value: value,
                mode: 1
            }})
            .then((res) => {
                if (res.value) return res.value
                throw {type:'internal', e: res.error}
            })
    },

    async getStatus (printerId) {
        return utils.fetch(`/printers/${printerId}/state`)
            .then((res) => {
                if (res.value) return toFancyPrinterStatus(res.value)
                throw {type: 'internal', e: res.error}
            })
    },

    async loadFile (printerId, fileId) {
        return utils.fetch(`/printers/${printerId}/load`, {method: 'POST', body: {
                fileid: fileId
            }
        })
            .then((res) => {
                if (res.value) return res.value
                throw {type:'internal', e: res.error}
                }
            )
    },

    async getJobs(printerId) {
        return utils.fetch(`/printers/${printerId}/jobs/history`, {method: 'GET'})
            .then((res) => {
                if (res.value) return toFancyJobs(res.value.job_history)
                throw {type: 'internal', e: res.error}
            })
    },

    async getFileDetails(printerId) {
        return utils.fetch(`/printers/${printerId}/state`)
            .then((res) => {
                if (res.value) return res.value
                throw {type: 'internal', e: res.error}
            })
    },

    async printerAction(printerId, action) {
        return utils.fetch(`/printers/${printerId}/${actions[action].path}`, {method: actions[action].method})
            .then((res) => {
                if (res.value) return res.value
                throw {type: 'internal', e: res.error}
            })
    }
}
