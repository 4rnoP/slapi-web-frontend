export default {
    async fetch(url, params) {
        if (params == null) {
            params = {}
        }
        if (!params.headers) {
            params.headers = {}
        }
        url = '/api'+url
        if (window.webpackHotUpdate) {
            url = `https://localhost:8080${url}`
        }
        params.headers = Object.assign(
            {Authorization: window.localStorage.getItem('token')},
            params.headers
        )
        if (params.body){
            if (params.body.constructor.name === 'Object') {
                params.headers['Content-Type'] = 'application/json'
                params.body = JSON.stringify(params.body)
            }
        }

        return fetch(url, params)
            .then(res => {
                if ((res.headers.get("Content-Type") || "").startsWith("application/json")) {
                    return res.json().then(json => {
                        return json
                    })
                } else {
                    return res.blob().then()
                }
            })
            .catch((e) => { //Fetch error
                throw {type: 'network', e: e}
            })

    }

}