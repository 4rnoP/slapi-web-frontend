export default {
    methods: {
        timeBetween(date1, date2) {
            let diff = Math.round(Math.abs(date2.getTime() - date1.getTime()))
            return this.stringifyTimespan(diff)
        },
        stringifyTimespan(diff) {
            diff = Math.round(diff/1000)
            if (diff<60) {
                return `${diff} second${diff>1 ? 's' : ''}`
            } else if (diff < 3600)
            return `${Math.floor(diff/60)} minute${diff>60 ? 's' : ''} and ${diff%60} second${diff%60 !== 0 ? 's' : ''}`
            diff = Math.floor(diff/60)
            return `${Math.floor(diff/60)} hour${diff>60 ? 's' : ''} and ${diff%60} minute${diff%60 !== 0 ? 's' : ''}`
        }
    }
}