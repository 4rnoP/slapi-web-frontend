const commonErrors = {
    'unknown error': 'A unknown error occurred',
    'invalid request': 'An invalid request was made to the server',
    'invalid token': 'Your session is invalid, so we’ve logged you out',
    'insufficient permissions': 'You don\'t have enough permissions',
    'unknown user': 'This user doesn\'t exists',
    'unknown printer': 'This printer doesn\'t exists',
    'not calibrating': 'The printer is not in calibration mode',
    'currently printing': 'The printer is currently in a print',
    'currently calibrating': 'The printer is currently in calibration mode',
    'last admin user': 'You are the last admin user',
    'no printer connection': 'Couldn\'t communicate with the printer',
    'not calibrated': 'The printer has not been calibrated yet',
    'not powered': 'The printer is not powered'
}

const genericErrors = {
    'internal': 'A internal error occurred',
    'network': 'A network error occurred',
    'user': 'It looks like you tried to make something that is forbidden'
}

export default {
    methods: {
        handleError (e) {
            console.error(e)
            let msg = e.text || commonErrors[e.e] || genericErrors[e.type] || commonErrors["unknown error"]
            if (e.e === 'invalid token') this.$store.dispatch('user/cleanLogout').then()
            this.$buefy.notification.open({
                duration: 4000,
                message: `<b>Error</b>: ${msg}`,
                type: 'is-danger'
            })
        },
        dispatchOrError (path, ...args) {
            this.$store.dispatch(path, ...args).catch(e => this.handleError(e))
        }
    }
}