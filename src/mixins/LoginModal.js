import Login from "@/components/Modals/Login"
export default {
    methods: {
        openLoginModal () {
            this.$buefy.modal.open({
                parent: this,
                component: Login,
                hasModalCard: true,
                trapFocus: true
            })
        }
    }
}