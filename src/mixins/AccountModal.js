import Account from "@/components/Modals/Account"
export default {
    methods: {
        openAccountModal () {
            this.$buefy.modal.open({
                parent: this,
                component: Account,
                hasModalCard: true,
                trapFocus: true
            })
        }
    }
}