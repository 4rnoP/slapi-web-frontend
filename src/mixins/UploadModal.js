import Upload from "@/components/Modals/UploadFile"
export default {
    methods: {
        openUploadModal (file) {
            this.$buefy.modal.open({
                parent: this,
                component: Upload,
                hasModalCard: true,
                trapFocus: true,
                props: {file: file}
            })
        }
    }
}