import Settings from "@/components/Modals/Settings"
export default {
    methods: {
        openSettingsModal () {
            this.$buefy.modal.open({
                parent: this,
                component: Settings,
                hasModalCard: true,
                trapFocus: true
            })
        }
    }
}