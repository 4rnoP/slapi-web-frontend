import Vue from 'vue';
import Vuex from 'vuex';
import files from './modules/files'
import printer from './modules/printer'
import user from './modules/user'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        files,
        printer,
        user
    }
});