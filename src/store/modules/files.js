import api from '@/api/files'

const state = {
    filesList: []
}

const getters = {
    file (state, id) {
        return state.filesList.find(item => item.id === id)
    },
    allTags (state) {
        return state.filesList
            .flatMap(file => file.tags ?? [])
            .filter((value, index, array) => array.indexOf(value) === index)
    }
}

const mutations = {
    setFiles (state, files) {
        state.filesList = files
    },
    renameFile(state, file, newName) {
        let index = state.filesList.findIndex(myfile => myfile.id === file.id)
        state.filesList[index].name = newName
    }
}

const actions = {
    updateFiles (context) {
        return api.getFiles()
            .then(newFiles => {
                context.commit("setFiles", newFiles)
                return newFiles
            })
    },

    deleteFile (context, file) {
        api.deleteFile(file.id).then(() =>
            context.dispatch('updateFiles')
        )
    },

    uploadFile(context, { file, name, tags }) {
        return api.uploadFile(file, name, tags).then(() => context.dispatch('updateFiles'))
    },

    downloadFile(context, file) {
        return api.downloadFile(file.id).then(
            fileDl => {
                try {
                    let a = document.createElement("a");
                    a.download = file.name;
                    a.hidden = true;
                    document.body.appendChild(a);
                    a.href = URL.createObjectURL(new File([fileDl], file.name,{'type':'application/zip'}))
                    a.click()
                    a.remove()
                } catch (e) {
                    throw {type: 'internal', e: e}
                }
            }
        )

    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}