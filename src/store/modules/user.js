import api from '@/api/user'

const state = {
    myID: null,
    myTokens: [],
    usersList: []
}

const getters = {
    myUser(state) {
        if (state.myID){
            return state.usersList.find((user) => (user.id === state.myID))
        }
    },
}

const mutations = {
    setID(state, id) {
        state.myID = id
    },
    setTokens(state, tokens) {
        state.myTokens = tokens
    },
    deleteUser(state) {
        state.myID = null
        state.myTokens = []
    },
    setUsersList(state, usersList) {
        state.usersList = usersList
    },
    deleteTokens(state) {
        state.myTokens = []
    }
}

const actions = {
    login(context, credentials={username:String, password:String, expire:-1, desc: 'A web browser'}) {
        return api.askToken(credentials.username, credentials.password, credentials.expire, credentials.desc)
            .then(res => {
                window.localStorage.setItem('token', res.token)
                context.commit('setID', res.id)
                return Promise.all([
                    context.dispatch('onceConnected'),
                    context.dispatch('getMyTokens'),
                ])
            })

    },

    checkToken(context){
        if (window.localStorage.getItem('token')) {
            return api.getUser('@me').then(user =>
                Promise.all([
                    context.commit("setID", user.id),
                    context.commit('setTokens', user.tokens),
                    context.dispatch("onceConnected").catch(e => {throw e})
                ]).then(() => true)
            ).catch((e) => {
                if (e.e === 'invalid token') {
                    context.dispatch('cleanLogout')
                    return false
                } else throw e
            })
        }
    },

    onceConnected(context){
        return Promise.all([
            context.dispatch('updateUsers'),
            context.dispatch('files/updateFiles', null, {root: true}).then(),
            context.dispatch('printer/fetchPrinterList', null, {root: true}).then()
        ])
     },

    logout(context) {
        return api.deleteToken().then(() => {context.dispatch('cleanLogout')}
        )
    },

    cleanLogout(context) {
        context.commit('deleteUser')
        window.localStorage.removeItem('token')
        context.commit('deleteTokens')
        context.commit('printer/resetCurrentPrinter', null, {root:true})
        context.commit('printer/setJobs', null, {root:true})
    },

    updateUsers(context){
        return api.getUsers().then(users => {
            context.commit('setUsersList', users)
            return users
        })
    },

    editUser(context, props){
        return api.editUser(props.id, {
            username: props.username,
            password: props.password,
            is_admin: props.is_admin
        }).then(() => context.dispatch("updateUsers"))
    },

    createUser(context, user){
        return api.createUser(user.username, user.password, user.is_admin).then(() => context.dispatch('updateUsers'))
    },

    getMyTokens(context, force=false){
        if (context.state.myTokens.length === 0 || force){
            return api.getUser('@me')
                .then((user) => context.commit('setTokens', user.tokens))
                .then(() => context.state.myTokens)
        } else return context.state.myTokens
    },

    deleteToken(context, tokenId){
        if (tokenId) return api.deleteTokenById({id:tokenId}).then(() => context.dispatch('getMyTokens', true))
        else return api.deleteToken().then(() => context.dispatch('getMyTokens', true))
    },

    deleteUser(context, id){
        return api.deleteUser(id).then(context.dispatch("updateUsers"))
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}