import api from '@/api/printer'

const state = {
    printerList: [],
    currentPrinter: null,
    printerStatus: {},
    jobsHistory: []
}

const getters = {
}

const mutations = {
    setPrinterList(state, printers) {
        state.printerList = printers
    },

    setCurrentPrinter(state, printerId) {
        state.currentPrinter = printerId
    },
    setStatus(state, status) {
        state.printerStatus = status
    },
    setJobs (state, jobs){
        state.jobsHistory = jobs
    },
    resetCurrentPrinter(state) {
        state.currentPrinter = null
        state.jobsHistory = []
        state.printerStatus = {}
    }
}

const actions = {
    fetchPrinterList(context) {
        return api.getPrinters()
            .then(printers => context.commit('setPrinterList', printers))
    },

    saveChanges(context, changedPrinter) {
        return api.editPrinter(changedPrinter.id, changedPrinter.name, changedPrinter.socket_address, changedPrinter.properties)
            .then(() => context.dispatch('fetchPrinterList'))
    },

    newPrinter(context, printer) {
        return api.newPrinter(printer.name, printer.socket_address, printer.properties)
            .then(() => context.dispatch('fetchPrinterList'))
    },

    deletePrinter(context, printerId) {
        return api.deletePrinter(printerId)
            .then(() => context.dispatch('fetchPrinterList'))
    },

    printerAction(context, action) {
        return api.printerAction(context.state.currentPrinter, action).then(() =>
            context.dispatch('getPrinterStatus', context.state.currentPrinter)
        )
    },

    loadFile(context, file) {
        return api.loadFile(context.state.currentPrinter, file.id).then(() =>{
            context.dispatch('getPrinterStatus', context.state.currentPrinter)}
        )
    },

    movePlatform(context, length) {
        return api.moveCalibration(context.state.currentPrinter, length)
    },

    getPrinterStatus(context, printerId) {
        if (!printerId) {
            printerId = context.state.currentPrinter
        }
        if (printerId) {
            return api.getStatus(printerId).then((status) => {
                context.commit('setStatus', status)
            })
        } throw {type: 'internal', e:'tried to get printer status without any specified printer nor current printer'}
    },

    updateJobs (context) {
        return api.getJobs(context.state.currentPrinter).then(jobs => {
            context.commit('setJobs', jobs)
            return jobs
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}