import Vue from 'vue'
import App from './components/App.vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import store from './store/index'
Vue.use(Buefy, {
  defaultIconPack: 'fa',
})

new Vue({
  el: 'body',
  store,
  render: h => h(App),
})
